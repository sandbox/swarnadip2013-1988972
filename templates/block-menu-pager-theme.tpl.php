<?php 
 /**
 * @file
 * Customize the display of a menu pager.
 *
 * Available variables:
 * - $variables: The menu names.
 */

$pagination = new Pagination();
$all_menu_name = $variables['item']['menu_name'];
$link_names = $pagination->getLinkNames($all_menu_name);


print "<div class='Menu-pager-div'>";
print "<ul>";
print "<li>" . $link_names['first_link'] . "</li>";
print "<li>" . $link_names['last_link'] . "</li>";
print "</ul>";
print "<div>";
