Description
-----------
This module adds a pagination for main menu to your Drupal site.This module can be used to traverse trough the pages of main menu using pagination. Users just need to click on the links to traverse into next page or into previous page of main menu navigation.

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire menu_pgaer directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module in the "Administer" -> "Modules"
3. Edit the settings under "Administer" -> "Configuration" ->
   "Menu Pagination" -> "Main menu pager settings"
4. Assign the block into Content region from "Administer" -> "Structure" ->
   "Blocks" -> "Menu Pager Block"
   
Support
--------
Please use the issue queue for filing bugs with this module.
