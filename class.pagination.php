<?php

/**
 * @file
 * Contains the function definations for menu pager
 * @author Swarnadip Banerjee <hibanerjee003@gmail.com>
 */

class Pagination 
{
  public $arrParentMenu;
  public $curPageNid;
  public $linkNames;
  public function __construct() {
    $this->arrParentMenu = array();
    $this->curPageNid = $_GET['q'];
    $this->linkNames = array();
  }
  /**
  * @function: recursive function for menu items
  */
  public function createMenuTreeArray($arrmenus) {
   if(is_array($arrmenus)) {
     foreach ($arrmenus as $key => &$value) {
      $this->arrParentMenu[$arrmenus[$key]['link']['link_path']]['link_title'] = $arrmenus[$key]['link']['link_title'];
      if(isset($value['below']) && !empty($value['below'])){
        $this->createMenuTreeArray($value['below']);
      }				
     }
   }
   return $this->arrParentMenu;		
  }
	
  /**
  * @function: get previous and next menu items from current item
  */
  public function getPrevNext($haystack,$needle) {
   $prev = $next = NULL;
   $akeys = array_keys($haystack);
   $k = array_search($needle,$akeys);
   if ($k !== FALSE) {
     if ($k > 0) {
       $prev = $akeys[$k-1];
	 }
     if ($k < count($akeys)-1) {
       $next = $akeys[$k+1];
	 }
    }
    return array($prev,$next);
  }
    
  /**
  * @function: get url alies of menu item
  */
  public function getUrlAlies($nid) {		
    $result = db_select('url_alias', 'u')
      ->fields('u', array('alias'))
      ->condition('source',$nid,'=')
      ->execute()		
      ->fetchAssoc();
    return $result;
  }
	
  /*
  * @function: get link title
  */
  public function getLinkTitle($arrmenutree, $curpageid) {
   if( $arrmenutree[$curpageid]['link_title'] != '' ) {
     return $arrmenutree[$curpageid]['link_title'];
   }
  }
	
  /**
  * @function: create link names for pagination
  */
  public function getLinkNames($menunamearray) {
    $arrnodes 		= $this->createMenuTreeArray($menunamearray);
    $arrprevnext 	= $this->getPrevNext($arrnodes,$this->curPageNid);
    $arrprev 		= $arrprevnext[0];
    $arrNext 		= $arrprevnext[1];
    if( $arrprev == '' ) {
      $firstpagetitle = $this->getLinkTitle($arrnodes,$this->curPageNid);
      $linkNames['first_link']	=	$firstpagetitle;
    }
    else {
      $pre_link = $this->getUrlAlies($arrprev);
      $prevpagetitle = $this->getLinkTitle($arrnodes,$arrprev);
      $linkNames['first_link']	= "<a href='".$pre_link['alias'] . "'>&lt;" . $prevpagetitle . "</a> ";
    }
    if( $arrNext == '' ) {
      $lastpagetitle = $this->getLinkTitle($arrnodes,$this->curPageNid);
      $linkNames['last_link'] = $lastpagetitle;
    }
    else {
      $next_link = $this->getUrlAlies($arrNext);
      $nextpagetitle = $this->getLinkTitle($arrnodes,$arrNext);
      $linkNames['last_link'] = "<a href='".$next_link['alias'] . "'>" . $nextpagetitle . "&gt;</a>";
    }
    return $linkNames;
  }
}
